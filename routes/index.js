const express = require('express');
const router = express.Router();
const users = require('../controllers/users');

router.post('/create', users.createUser);

module.exports = router;