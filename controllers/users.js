const users = require("../models").users;

exports.createUser = async (req, res) => {
  const { firstname, lastname, email, pseudo } = req.body;

  if (!firstname)
    return res.status(400).send("Le prénom est obligatoire.");
  if (!lastname)
    return res.status(400).send("Le nom est obligatoire.");
  if (!email)
    return res.status(400).send("L'adresse e-mail est obligatoire.");
  if (pseudo !== undefined) {
    const ifPseudoExist = await users.findOne({ where: { pseudo: pseudo } });
    if (ifPseudoExist)
      return res.status(400).send("Le pseudo est déjà utilisé, veuillez en choisir un autre.");
  }
  const ifEmailExist = await users.findOne({ where: { email: email } });
  if (ifEmailExist)
    return res.status(400).send("Cet email est déjà lié à un compte.");

  return users.create({
    firstname: firstname,
    lastname: lastname,
    email: email,
    pseudo: pseudo
  }).then(results => res.status(200).send(results))
    .catch(err => res.status(200).send(err))
}